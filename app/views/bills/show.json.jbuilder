json.restaurant @bill.table.restaurant.name
json.address @bill.table.restaurant.address
json.waiter current_user.role
json.orders do
	json.array! @orders do |order|
	  json.id order.id.to_s
	  json.item order.item.name
	  json.price order.item.price
	  json.quantity order.quantity
	  json.total order.total
	end
end
json.total_quantity @bill.total_quantity
json.grand_total @bill.grand_total
json.vat_of_percent @bill.vat.to_s + '%'
json.vat_amount @bill.vat_amount
json.net_amount @bill.amount_with_vat