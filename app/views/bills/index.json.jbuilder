json.restaurant @restaurant.name
json.address @restaurant.address
json.waiter current_user.role
json.array! @bills do |bill|
  json.table do
    json.id bill.table.id.to_s
    json.table bill.table.table_number
  end
  json.bills do
    json.id bill.id.to_s
    json.extract! bill, :bill_no 
    json.orders do
			json.array! bill.orders do |order|
			  json.id order.id.to_s
			  json.item order.item.name
			  json.price order.item.price
			  json.quantity order.quantity
			  json.total order.total
			end
		end
	end
end
