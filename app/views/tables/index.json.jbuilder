json.array! @tables do |table|
  json.id table.id.to_s
  json.extract! table, :table_number
end