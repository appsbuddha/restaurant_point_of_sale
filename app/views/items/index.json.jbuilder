json.array! @items do |item|
  json.id item.id.to_s
  json.extract! item, :name, :price, :category
  json.image do
    json.url item.image? ? item.image_url : ''
  end
end	