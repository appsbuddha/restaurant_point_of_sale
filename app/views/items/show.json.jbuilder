json.extract! @item, :id, :name, :price, :category
json.image do
  json.url @item.image? ? @item.image_url : ''
end