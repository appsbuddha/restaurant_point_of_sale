json.array! @users do |user|
  json.id user.id.to_s
  json.extract! user, :name, :email, :phone, :role
end	