json.array! @restaurants do |restaurant|
  json.id restaurant.id.to_s
  json.extract! restaurant, :name, :phone, :address
  json.image do
    json.url restaurant.logo_image? ? restaurant.image_url : ''
  end
end	