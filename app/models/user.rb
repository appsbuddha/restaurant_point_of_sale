class User
	include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword

  field :name, type: String
  field :email, type: String
  field :password_digest, type: String
  field :phone, type: String
  field :role, type: String
  field :deleted, type: Mongoid::Boolean, default: false

  has_secure_password

  belongs_to :restaurant

  validates :name, :phone, :role, presence: true
  validates :password, presence: true, on: :create
  validates :phone, uniqueness: {scope: :restaurant, message: ' allready registerd'}
end