class Table
	include Mongoid::Document
  include Mongoid::Timestamps
  
  field :table_number, type: Integer

  belongs_to :restaurant
  has_many :orders
  has_many :bills

  validates :table_number, uniqueness: {scope: :restaurant, message: 'Table is available'}
end