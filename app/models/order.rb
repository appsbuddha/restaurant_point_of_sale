class Order
	include Mongoid::Document
  include Mongoid::Timestamps
   
  field :quantity, type: Integer
  field :cancle, type: Mongoid::Boolean, default: false
  field :paid_at, type: DateTime
  field :total, type: Integer
   
  belongs_to :table
  belongs_to :item
  belongs_to :user
end