class Item
	include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :price, type: Integer
  field :category, type: String

  mount_uploader :image, ImageUploader

  has_many :orders
  belongs_to :restaurant
  validates :name, :price, presence: true
  validates :name, uniqueness: {scope: :restaurant, message: "Item is allready available"}
end