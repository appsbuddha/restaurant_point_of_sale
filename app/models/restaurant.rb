class Restaurant
	include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :address, type: String
  field :phone, type: String
  field :deleted, type: Mongoid::Boolean, default: false

  mount_uploader :logo_image, ImageUploader

  has_many :tables
  has_many :items
  has_many :users

  validates :name, :address, :phone, presence: true
  validates :name, uniqueness: {scope: :name, message: ' allready registerd'}

end