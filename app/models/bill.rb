class Bill
	include Mongoid::Document
  include Mongoid::Timestamps

  field :vat, type: Integer
  field :bill_no, type: String
  field :order_ids, type: Array, default: []

  belongs_to :table
  belongs_to :user

  validates :vat, :bill_no, presence: true

  def amount_with_vat
    orders = Order.where({id: {"$in" => self.order_ids}})
    total_amount = (orders.sum(:total).to_i) + self.vat_amount
  end

  def grand_total
    orders = Order.where({id: {"$in" => self.order_ids}})
    orders.sum(:total)
  end

  def vat_amount
    orders = Order.where({id: {"$in" => self.order_ids}})
    vat_amount = (orders.sum(:total).to_i * self.vat)/ 100
  end

  def total_quantity
    orders = Order.where({id: {"$in" => self.order_ids}})
    sum = orders.sum(:quantity)
  end

  def orders
    orders = Order.where({id: {"$in" => self.order_ids}})
  end
end