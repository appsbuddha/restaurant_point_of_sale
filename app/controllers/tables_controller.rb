class TablesController < ApplicationController

	protect_from_forgery :except => [:create, :index, :delete_table]
  before_filter :authorize_admin, only:[:create, :delete_table]
	def index
		@tables = Table.where(restaurant_id: params[:restaurant_id])
	end

	def show
		@table = Table.where({"$and" => [{id: params[:id]}, {restaurant_id: params[:restaurant_id]}]}).first
		unless @table
			render status: :precondition_failed, json: {message: 'Table not found'}
		end
	end

  def create
  	tables = Table.where(restaurant_id: params[:restaurant_id])
  	unless tables.blank?
  		@tbl = tables.last.table_number
  	else
  		@tbl = 0
  	end
  	table_count = @tbl
  	params[:number].to_i.times do
  		table_count = table_count + 1
			@table = Table.create(table_number: table_count, restaurant_id: params[:restaurant_id])
	  end
	  unless @table.errors.any?
			render status: :ok, json: {message: 'Tables created'}
		else
			render status: :precondition_failed, json: {error: true, message: @table.errors.full_messages}
		end
	end

	def delete_table
		num = 0
		tables = Table.where({"$and" => [{id: {"$in" => params[:ids]}}, {restaurant_id: params[:restaurant_id]}]}).order("table_number ASC")
		tables.each do |table|	
			if table.orders.count > 0
			else
				@last_table = table.table_number.to_i - num
		    new_tables = Table.where({table_number: {"$gt" => @last_table}})
		    table.destroy
		    if !new_tables.blank?
			  	new_tables.each do |table|
			  		table.update(table_number: table.table_number.to_i - 1)
			  	end
	      end
	      num = num + 1
		  end
	  end
	  
	  render status: :ok, json: {message: 'Tables deleted'}
	end

	private

	def authorize_admin
		if current_user.role.eql?("super admin") or current_user.role.eql?("owner")
			return true
		else
			render status: :precondition_failed, json: {error: true, message: 'You are not authorize to restaurant data'}
		end
	end
end