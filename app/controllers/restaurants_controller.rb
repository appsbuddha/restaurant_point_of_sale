class RestaurantsController < ApplicationController

	protect_from_forgery :except => [:create, :update, :delete]
	before_filter :authorize_admin

	def index
		@restaurants = Restaurant.where(deleted: false) 
	end

	def create
    @restaurant = Restaurant.create(restaurant_params)
    unless @restaurant.errors.any?
			render status: :ok, json: {id: @restaurant.id.to_s, message: 'Restaurant created'}
		else
			render status: :precondition_failed, json: {error: true, message: @restaurant.errors.full_messages}
		end
	end

	def update
		restaurant = Restaurant.where(id: params[:id]).first
		if restaurant
			restaurant.update(restaurant_params)
			unless restaurant.errors.any?
			  render status: :ok, json: {id: restaurant.id.to_s, message: 'Restaurant Updated'}
		  else
			  render status: :precondition_failed, json: {error: true, message: restaurant.errors.full_messages}
		  end
		else
			render status: :not_found, json: {error: true, message: "Restaurant not found"}
		end
	end

	def destroy
		restaurant = Restaurant.where(id: params[:id]).first
		if restaurant
			restaurant.update(deleted: true)
			unless restaurant.errors.any?
			  render status: :ok, json: {id: restaurant.id.to_s, message: 'Restaurant deleted'}
		  else
			  render status: :precondition_failed, json: {error: true, message: restaurant.errors.full_messages}
		  end
		else
			render status: :not_found, json: {error: true, message: "Restaurant not found"}
		end
	end

	private

	def restaurant_params
		params.require(:restaurant).permit(:name, :phone, :address, :deleted)
	end

	def authorize_admin
		if current_user.role.eql?("super admin")
			return true
		else
			render status: :precondition_failed, json: {error: true, message: 'You are not authorize to restaurant data'}
		end
	end
end