class OrdersController < ApplicationController

	protect_from_forgery :except => [:create, :cancle]

	def index
		@orders = Order.where({"$and" => [{table_id: params[:table_id]},{paid_at: nil}]})
	end	

	def create
		item = Item.where(id: params[:item_id]).first
		total_price = params[:quantity].to_i * item.price
	  @order = Order.create(order_params.merge(table_id: params[:table_id], user_id: current_user.id, item_id: params[:order][:item_id], total: total_price))
	  if @order.errors.any?
	  	render status: :precondition_failed, json: {error: true, message: @order.errors.full_messages}
	  else
	  	render status: :ok, json: {id: @order.id, message: 'Order created'}
	  end
	end

	def cancel
		order = Order.where(id: params[:id]).first
		if order
			order.update(cancle: true)
			render status: :ok, json: {id: order.id.to_s, message: 'Order cancled'}
		else
			render status: :precondition_failed, json: {error: true, message: 'Order is not find'}
		end
	end

	private

	def order_params
		params.require(:order).permit(:quantity)
	end
end