class BillsController < ApplicationController

	protect_from_forgery :except => [:create, :index]

	def index
		@bills = Bill.all
		@restaurant = Restaurant.first
	end

	def show
		@bill = Bill.where({"$and" => [{id: params[:id]}, {table_id: params[:table_id]}]}).first
		@orders = Order.where({id: {"$in" => @bill.order_ids}})
	end

	def create
    bill = Bill.create(vat: params[:bill][:vat], bill_no: params[:bill][:bill_no], order_ids: params[:bill][:order_ids], table_id: params[:table_id], user_id: current_user.id)
    if bill.errors.any?
    	render status: :precondition_failed, json: {error: true, message: bill.errors.full_messages}
    else
    	orders = Order.where({id: {"$in" => params[:bill][:order_ids]}})
    	orders.update_all(paid_at: Date.today)
    	render status: :ok, json: {message: 'Will is created'}
    end
	end
end