class ItemsController < ApplicationController

	protect_from_forgery :except => [:create]
	before_filter :authorize_admin, only: [:create, :update, :destroy]

	def index
		@items = Item.where(restaurant_id: params[:restaurant_id])
	end

	def show
		@item = Item.where({"$and" => [{id: params[:id]}, {restaurant_id: params[:restaurant_id]}]}).first
		unless @item
			render status: :precondition_failed, json: {message: 'Item not found'}
		end
	end

	def create
		@item = Item.create(item_params.merge(restaurant_id: params[:restaurant_id]))
		unless @item.errors.any?
			render status: :ok, json: {id: @item.id.to_s, message: 'Item created'}
		else
			render status: :precondition_failed, json: {error: true, message: @item.errors.full_messages}
		end
	end

	def update
		item = Item.where({"$and" => [{id: params[:id]}, {restaurant_id: params[:restaurant_id]}]}).first
		if item
			item.update(item_params)
			unless item.errors.any?
				render status: :ok, json: {id: item.id.to_s, message: 'Item Updated'}
			else
				render status: :precondition_failed, json: {error: true, message: item.errors.full_messages}
			end
		else
			render status: :not_found, json: {error: true, message: "Item not find"}
		end
	end

	def destroy
		item = Item.where({"$and" => [{id: params[:id]}, {restaurant_id: params[:restaurant_id]}]}).first
		if item
      item.destroy
      render status: :ok, json: {message: 'Item Deleted'}
    else
    	render status: :not_found, json: {errors: true, message: 'Item not find'}
    end
	end

	private

	def item_params
		params.require(:item).permit(:name, :price, :category)
	end

	def authorize_admin
		if current_user.role.eql?("super admin") or current_user.role.eql?("owner")
			return true
		else
			render status: :precondition_failed, json: {error: true, message: 'You are not authorize to restaurant data'}
		end
	end
end