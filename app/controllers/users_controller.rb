class UsersController < ApplicationController
  
  protect_from_forgery :except => [:create, :login]
  before_filter :authorize_admin, only: [:create, :update, :destroy]
   
  def index
  	@users = User.where({"$and" => [{restaurant_id: params[:restaurant_id]}, {deleted: false}]})
  end

	def create
		if !params[:user][:role].eql?("owner") && current_user.role.eql?("owner")
			user = User.create(user_params.merge(password: params[:password], restaurant_id: params[:restaurant_id]))
			unless user.errors.any?
				render status: :ok, json: {id: user.id, message: 'User created'}
			else
				render status: :precondition_failed, json: {error: true, message: user.errors.full_messages}
			end
	  elsif current_user.role.eql?("super admin")
	  	user = User.create(user_params.merge(password: params[:password],restaurant_id: params[:restaurant_id]))
	  else
	  	render status: :precondition_failed, json: {error: true, message: 'You are not authorize person'}
	  end
	end

	def show
		@user = User.where({"$and" => [{id: params[:id]}, {restaurant_id: params[:restaurant_id]}, {deleted: false}]}).first
		unless @user
			render status: :not_found, json: {message: 'User not found'}
		end
	end

	def login
		user = User.where(email: params[:email]).first || User.where(phone: params[:phone]).first
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      render status: :ok,
             json: { message: "Login successfull",
                     payload: {
                       id: user.id.to_s,
                       name: user.name.titleize,
                       email: user.email}
                   }
    else
      render status: 401,
             json: {
               error: true,
               message: "Invalid email or password."
             }
    end
	end

	def logout
		session[:user_id] = nil
    render status: :ok, json: {message: 'Logout Successfuly'}
  end

	def update
		user = User.where({"$and" => [{id: params[:id]}, {restaurant_id: params[:restaurant_id]}]}).first
		if user 
			user.update(user_params)
			unless user.errors.any?
				render status: :ok, json: {id: user.id.to_s, message: 'User Updated'}
			else
				render status: :precondition_failed, json: {error: true, message: user.errors.full_messages}
			end
		else
			render status: :not_found, json: {error: true, message: 'User not find'}
		end
	end

	def destroy
		user = User.where({"$and" => [{id: params[:id]}, {restaurant_id: params[:restaurant_id]}]}).first
		if user
      user.update(deleted: true)
      render status: :ok, json: {message: 'User Deleted'}
    else
    	render status: :not_found, json: {errors: true, message: 'User not found'}
    end
	end

	private

	def user_params
		params.require(:user).permit(:name, :email, :password, :role, :phone)
	end

	def authorize_admin
		if current_user.role.eql?("super admin") or current_user.role.eql?("owner")
			return true
		else
			render status: :precondition_failed, json: {error: true, message: 'You are not authorize to restaurant data'}
		end
	end
end