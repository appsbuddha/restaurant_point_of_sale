class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user
  before_filter :authorise_request, except: [:welcome]

  def welcome
    render text: :helloworld
  end

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user
    end
  end

  def authorise_request
    @current_user ||= User.where(phone: params[:token]).first 
    if @current_user.nil?
      render status: 401, json: { error:  true, message: "Unauthorized Request. Valid token required with the request."} and return
    end
  end
end
