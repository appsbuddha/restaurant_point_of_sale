Rails.application.routes.draw do
  root 'application#welcome'

  resources :restaurants do
    resources :users, only: [:create, :update, :show, :destroy, :index]
    resources :tables, only: [:create, :index, :destroy, :show] do
      delete :delete_table, on: :collection
    end
    resources :items, only: [:create, :index, :update, :show, :destroy]
  end

  resources :users, only:[] do
  	post :login, on: :collection
  	get :logout, on: :collection
  end 

  resources :tables, only: [] do
  	resources :orders, only: [:index, :create] do
  		put :cancel, on: :member
  	end
    resources :bills, only: [:show, :create]
  end

  resources :bills, only: [:index]
end
